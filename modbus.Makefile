where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



USR_CFLAGS   += -DUSE_TYPED_RSET
USR_CPPFLAGS += -DUSE_TYPED_RSET

APP:=modbusApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

HEADERS   += $(APPSRC)/drvModbusAsyn.h
HEADERS   += $(APPSRC)/modbusInterpose.h
HEADERS   += $(APPSRC)/modbus.h

SOURCES   += $(APPSRC)/drvModbusAsyn.cpp
SOURCES   += $(APPSRC)/modbusInterpose.c

# Cannot handle *Include.dbd file
#SOURCES   += $(APPSRC)/testModbusSyncIO.cpp
#DBDEXPANDPATH += $(E3_SITEMODS_PATH)/asyn/$(ASYN_DEP_VERSION)/dbd
#DBDS      += $(APPSRC)/modbusInclude.dbd

DBDS      += $(APPSRC)/modbusSupport.dbd

TEMPLATES += $(wildcard $(APPDB)/*.template)

SCRIPTS += ../iocsh/modbus_s7plc.iocsh


# EPICS_BASE_HOST_BIN = $(EPICS_BASE)/bin/$(EPICS_HOST_ARCH)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I$(EPICS_BASE)/db
USR_DBFLAGS += -I$(APPDB)


$(TEMS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@


.PHONY: vlibs
vlibs:
#
